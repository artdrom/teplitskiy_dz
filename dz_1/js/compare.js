
    let Input1 = document.querySelector('.number1');

    let Input2 = document.querySelector('.number2');

    let Input3 = document.querySelector('.number3');

    let btnResult = document.querySelector('.btnResult');


btnResult.onclick = function () {

    let valInput1 = Input1.value.trim();

    let valInput2 = Input2.value.trim();

    let valInput3 = Input3.value.trim();




    if(valInput1.length === 0
       || valInput2.length === 0
       || valInput3.length === 0) {
        alert('Вы не ввели значение!');
        return;
    }

    valInput1 = Number(valInput1);

    valInput2 = Number(valInput2);

    valInput3 = Number(valInput3);

    if(valInput1 === valInput2
       || valInput1 === valInput3
       || valInput2 === valInput3){
        alert('Вы ввели одинаковые числа. Исправьте ошибку!');
        return;
    }

    if(isNaN(valInput1)
       || isNaN(valInput2)
       || isNaN(valInput3)){
        alert('Вы ввели некорректное значение! Вводить можно только числа');
        return;
    }
//первый способ
    let maxNum = valInput1;

    let minNum = valInput1;

    let amountAll = valInput1 + valInput2 + valInput3;

    let mediumNum;

    if(maxNum < valInput2){

        maxNum = valInput2;
    }
    else{

        minNum = valInput2;
    }

    if(maxNum < valInput3){

        maxNum = valInput3;
    }

    if(minNum > valInput3){

        minNum = valInput3;
    }

    mediumNum = amountAll - (minNum + maxNum);

//второй способ
   /*

   if(valInput1 > valInput2 && valInput1 < valInput3
       || valInput1 > valInput3 && valInput1 < valInput2) {
        mediumNum = valInput1;
    }

    if(valInput2 > valInput1 && valInput2 < valInput3
       || valInput2 > valInput3 && valInput2 < valInput1) {
        mediumNum = valInput2;
    }

    if(valInput3 > valInput1 && valInput3 < valInput2
       || valInput3 > valInput2 && valInput3 < valInput1) {
        mediumNum = valInput3;
    }
    */

    document.querySelector('.result').innerHTML = mediumNum;

};
