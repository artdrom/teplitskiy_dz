
const divForTowns = document.querySelector('.allCity');
let town = {};
let nameTown = ['Москва'];
const startCity = 0;
let indexCity = startCity;
const startNuberPeoples = 4;
let numberPeoples = startNuberPeoples;
let nameButton = ['delete', 'add', 'color', 'add city'];
let buttonPopup = document.querySelector('.button');
let popUp = document.querySelector('.contPopUp');


//Выведение popup
const visiblePopup = () => {
    popUp.classList.add("displayBlockPopup");
};

//Получение значения инпута
const getCityName = () => {
    let inputForm = document.querySelector('.cityInput');
    nameTown[nameTown.length] = inputForm.value;
    popUp.classList.remove("displayBlockPopup");
};

//Удаление кнопки city по клику
const deleteButtonCity = (index, allButtonConteiner) => {
    let maxBtn;

    for (button in town[index].buttons) {
        maxBtn = button;
    }

    delete town[index].buttons[Number(maxBtn)];
    allButtonConteiner.lastChild.remove();
};

// Функция генерации цвета
const getRandomColor = () => {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
};

// Функция генерации города в массиве
const createTown = (idCity, startCountPeoples) => {
    let id = idCity + 1;
    town[id] = {
            indexTown: id,
                class: "contAllCity",
                 name: nameTown[id - 1],
              peoples: {},
              buttons: {},
    };
    createPeoples(id, startCountPeoples);
    createAllBottom(id);
};

//функция подсчета человечков в конкретном массиве
const countPeoplesForArray = (idCity) => {
    let numberMaxPeoples;

    for (countPeoples in town[idCity].peoples) {
        numberMaxPeoples = countPeoples;
    }
    return numberMaxPeoples;
};

// Функция генерации всех человечков в массиве
const createPeoples = (idCity, startCountPeoples) => {

    for (let i = 1; i <= startCountPeoples; i++) {
        town[idCity].peoples[i] = {
            indexPeople: i,
                  color: getRandomColor()
        }
    }
};

// Функция генерации одного человечка массиве
const createOnePeople = (idCity, countPeoples) => {
    let newCountPeople = Number(countPeoples) + 1;

    for (let i = 1; i <= newCountPeople; i++) {

        if (i === newCountPeople) {
            town[idCity].peoples[i] = {
                indexPeople: i,
                      color: getRandomColor()
            }
        }
    }
    numberPeoples = newCountPeople;
};

// Функция создания кнопок в массиве
const createAllBottom = (idCity) => {

    for (let i = 1; i < nameButton.length; i++) {
            town[idCity].buttons[i] = {
                indexButtom: i,
                       name: nameButton[i - 1]
            }
    }
};

// Функция создания кнопоки в массиве
const createCityBottom = (idCity) => {

    for (let i = 1; i <= nameButton.length; i++) {

        if (i === nameButton.length) {
            town[idCity].buttons[i] = {
                indexButtom: i,
                name: nameButton[i - 1]
            }
        }
    }
};

// Функция обработки кликов на человечках левой и правой кнопками мыши
const clickedByPeople = (e) => {
        let idTown = e.path[1].dataset.id;
        let peopleConteiner = e.path[1];
        let maxNumberPeoples = Number(countPeoplesForArray(idTown));

        if (e.which === 3) {
            window.oncontextmenu = (function(e) {
                e.preventDefault();
                if (maxNumberPeoples > 1) {
                    deletePeople(idTown, peopleConteiner);
                    numberPeoples -= 1;
                }
                else {
                    alert('в городе не может быть меньше одного человека');
                }
            });
        }

        if (e.which === 1) {
            e.target.style.color = getRandomColor();
        }
};

// Функция обработки кликов на кнопки
const clickButton = (e) => {
    e.preventDefault();
    let idTown = e.path[0].dataset.id;
    let nameButton = e.path[0].dataset.name;
    let divAllPeoples = e.path[3].firstChild;
    let divAllButton = e.path[2];
    let maxNumberPeoples = Number(countPeoplesForArray(idTown));

    switch (nameButton) {

        case 'delete':

            if (maxNumberPeoples > 1) {
                deletePeople(idTown, divAllPeoples);
                numberPeoples -= 1;
            }
            else {
                alert('в городе не может быть меньше одного человека');
            }
            break;

        case 'add':

            if (maxNumberPeoples < 8) {
                createHtmlForPeople(idTown, divAllPeoples);
            }
            else if (numberPeoples === 8 && town[idTown].buttons[4] === undefined) {
                visiblePopup();
                createCityBottom(idTown);
                createHtmlForButton(idTown, divAllButton);
            }
            break;

        case 'color':
            colorForAllPeoples(maxNumberPeoples, divAllPeoples);
            break;

        case 'add city':
            createHtmlForTown(indexCity, startNuberPeoples, deleteButtonCity(idTown, divAllButton));
            break;
    }
};

// Создание дефолтного HTML для города  и вывод на экран
const createHtmlForTown = (index, startNuberPeoples) => {
    let indexTown = index + 1;
    let divForTown = document.createElement('div');
    divForTowns.className = 'city-container';

    let allPeoplesConteiner = document.createElement('div');//создание div для всех человечков
    allPeoplesConteiner.className = 'peoples-container';
    createTown(index, startNuberPeoples);

    let nameCityConteiner = document.createElement('div');
    nameCityConteiner.className = 'name-container';
    let nameCity = document.createElement('p');
    nameCity.innerHTML = town[indexTown].name;
    nameCity.className = 'nameTown';
    nameCityConteiner.appendChild(nameCity);
    allPeoplesConteiner.appendChild(nameCityConteiner);
    allPeoplesConteiner.setAttribute('data-id', town[indexTown].indexTown);

    for ( let i = 1; i <= startNuberPeoples; i++ ) {
        let peopleConteiner = document.createElement('i');//создание тега 'i' для всех человечков
        peopleConteiner.className = 'fas fa-male';
        peopleConteiner.setAttribute('style', 'color: ' + town[indexTown].peoples[i].color);

        peopleConteiner.addEventListener('mousedown', clickedByPeople);//евент клик на человечках
        allPeoplesConteiner.appendChild(peopleConteiner);
        town[indexTown].peoples[i].htmlPeople = peopleConteiner;
    }

    divForTown.appendChild(allPeoplesConteiner);
    let divForButton = document.createElement('div');//создание div всех кнопок
    divForButton.className = 'button-container';

    for (let i = 1; i < nameButton.length; i++) {
        let buttonDiv = document.createElement('div');//div кнопоки
        buttonDiv.className = 'cityPopulation';

        let buttonName = document.createElement('a');
        buttonName.className = 'cityPopulationA';
        buttonName.setAttribute('href', '#');
        buttonName.setAttribute('data-name', town[indexTown].buttons[i].name);
        buttonName.setAttribute('data-id', town[indexTown].indexTown);
        buttonName.innerHTML = town[indexTown].buttons[i].name;

        buttonName.addEventListener('click', clickButton);
        buttonDiv.appendChild(buttonName);
        divForButton.appendChild(buttonDiv);
        town[indexTown].buttons[i].htmlButton = buttonDiv;
    }

    divForTown.appendChild(divForButton);
    indexCity = indexTown;
    town[indexTown].htmlCity = divForTown;
    divForTowns.appendChild(divForTown);
};

// Создание HTML для человечка и вывод на экран
const createHtmlForPeople = (index, allPeoplesDiv) => {
    let maxNumberPeoples = countPeoplesForArray(index);
    createOnePeople(index, maxNumberPeoples);

    for ( let i = 1; i <= numberPeoples; i++ ) {

        if (i === numberPeoples) {
            let peopleConteiner = document.createElement('i');//создание тега 'i' для человечка
            peopleConteiner.className = 'fas fa-male';
            peopleConteiner.style.color = town[index].peoples[numberPeoples].color;
            peopleConteiner.addEventListener('mousedown', clickedByPeople);//евент клик на человечках
            allPeoplesDiv.appendChild(peopleConteiner);
            town[index].peoples[numberPeoples].htmlPeople = peopleConteiner;
        }
    }
};

// Создание HTML для кнопки citi и вывод на экран
const createHtmlForButton = (indexTown, allButtonDiv) => {

    for ( let i = 1; i <= nameButton.length; i++ ) {

        if (i === nameButton.length) {
            let buttonDiv = document.createElement('div');//div кнопоки
            buttonDiv.className = 'cityPopulation';

            let buttonName = document.createElement('a');
            buttonName.className = 'cityPopulationA';
            buttonName.setAttribute('href', '#');
            buttonName.setAttribute('data-name', town[indexTown].buttons[i].name);
            buttonName.setAttribute('data-id', town[indexTown].indexTown);
            buttonName.innerHTML = town[indexTown].buttons[i].name;

            buttonName.addEventListener('click', clickButton);
            buttonDiv.appendChild(buttonName);
            allButtonDiv.appendChild(buttonDiv);
            town[indexTown].buttons[i].htmlButton = buttonDiv;
        }
    }
};

// Удаление человечка из города
const deletePeople = (index, allPeoplesDiv) => {
    let maxNumberPeoples = Number(countPeoplesForArray(index));
    allPeoplesDiv.lastChild.remove();
    delete town[index].peoples[maxNumberPeoples];
    numberPeoples = maxNumberPeoples - 1;
};

//Изменение цвета по клику на кнопке color
const colorForAllPeoples = (maxNumberPeoples, divAllPeoples) => {
    for (let i = 1; i <= maxNumberPeoples; i++) {
        divAllPeoples.children[i].style.color = getRandomColor();
    }
};

let initCity = () => {
    createHtmlForTown(indexCity, startNuberPeoples);
    buttonPopup.addEventListener('click', getCityName);
};

document.addEventListener('DOMContentLoaded', initCity);
