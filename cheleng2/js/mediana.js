
const btnResult = document.querySelector('.btnResult');
const inp = document.querySelector('.num input');

function inpVal() {
    let inputValue = inp.value.trim().split(',');
    return inputValue;
}

function median(arrayNumber) {
    let median;
    let maxNumber = Number(arrayNumber[0]);
    let minNumber = Number(arrayNumber[0]);

    for(let i = 0; i < arrayNumber.length; i++) {
        if(Number(arrayNumber[i]) > maxNumber) {
           maxNumber =  Number(arrayNumber[i]);
        }
    }

    for(let i = 0; i < arrayNumber.length; i++) {
        if(Number(arrayNumber[i]) < minNumber) {
            minNumber =  Number(arrayNumber[i]);
        }
    }

    let sortArrayNamber = [];

    for(let j = minNumber; j <= maxNumber; j++) {
        for(let i = 0; i < arrayNumber.length; i++) {
            if(Number(arrayNumber[i]) === j) {
                sortArrayNamber.push(Number(arrayNumber[i]));
            }
        }
    }

    if(Number.isInteger(sortArrayNamber.length / 2)) {
        let medianIndex = (sortArrayNamber.length) / 2;
        median = (sortArrayNamber[medianIndex] + sortArrayNamber[medianIndex - 1]) / 2;
    }
    else{
        let medianIndex = (sortArrayNamber.length - 1) / 2;
        median = sortArrayNamber[medianIndex];
    }
    return median;
}

function printMediana(number) {
    let contPrint  = document.querySelector('.result');
    contPrint.innerHTML = number;
}

btnResult.onclick = () => {

    if(inp.value.trim().length === 0) {
        alert('введите числа');
        return;
    }
    let arrInp = inpVal();

    if(arrInp.length < 2) {
        alert('введите больше 1 числа');
        return;
    }
    let resultMedian = median(arrInp);
    printMediana(resultMedian);
};
