
 const btnResult = document.querySelectorAll('.btnResult');
 const minus = 'minus';

 //функция совершения арифметического действия
 function functArifmetic(valueInput, action, fixNum) {

     if (valueInput === fixNum) {

         return valueInput = 0;
     }

     if (action) {

         return valueInput += 1;
     }

     if (valueInput === 0) {

         return valueInput = 0;
     }

     return valueInput -= 1;
 }

 //функция проверки действия кнопки
 function functArifmeticOperator(dataMines, inputData) {

     if (inputData === dataMines) {

         return false;
     }

     return true;
 }

 //функция вывода в инпут
 function functPrintHtml(input, number) {
     input.value = number;
 }

 //событие клик
 for (let i = 0; i < btnResult.length; i++) {

     btnResult[i].addEventListener('click', (e) => {
         let input = e.target.parentNode.parentNode.parentNode.childNodes[1];
         let valueData = e.target.parentNode.attributes['data-action'].nodeValue;
         let counter = Number(input.value);

         functPrintHtml(input, functArifmetic(counter, functArifmeticOperator(minus, valueData), 10));
     });
 }