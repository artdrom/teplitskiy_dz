
 const btnResult = document.querySelectorAll('.btnResult');
 const input = document.querySelector('.placeText');
 const minus = 'minus';

 //функция совершения арифметического действия
 function functArifmetic(valueInput, action) {

     if (!action) {
         return valueInput - 1;
     }

     return valueInput + 1;
 }

 //функция проверки действия кнопки
 function functArifmeticOperator(dataMines, inputData) {

     if (inputData === dataMines) {
         return false;
     }

     return true;
 }

 //функция вывода в инпут
 function functPrintHtml(input, number) {
     input.value = number;
 }

 //событие клик
 for (let i = 0; i < btnResult.length; i++) {

     btnResult[i].addEventListener('click', function () {
         let valueData = this.attributes['data-action'].nodeValue;
         let valueInput = Number(input.value);

         if (valueInput === 0 && valueData === minus) {
             alert('Значение равно 0. Пока можно только прибавлять');
             return;
         }

         functPrintHtml(input, functArifmetic(valueInput, functArifmeticOperator(minus, valueData)));
     });
 }