
const slaider = {
        tagSlaids: $('.inerSladerDiv'),
     tagContImage: $('.slaide'),
        imgSlaids: $('.slaide img'),
          buttons: $('.btn'),
      numerSlaide: $('.numerSlaide p')
};

let numberCounter = slaider.tagContImage.length - 1;
let counter = numberCounter;

const numberSlaid = (counter) => {
    slaider.numerSlaide[0].innerHTML = counter;
};

numberSlaid(counter);

const maxHeightImgSlaid = (tagPict) => {
    let heightPict = 0;

    tagPict.each( function() {

        if (heightPict < this.height)
            heightPict = this.height;
        });

    return heightPict;
};

const maxWidthImgSlaid = (tagPict) => {
    let widthPict = 0;

    tagPict.each( function() {

        if (widthPict < this.height)
            widthPict = this.height;
        });

    return widthPict;
};

const delegateHeight = (tagSlaidConteiner, heightPict) => {

    tagSlaidConteiner.each( function () {
        $(this).height(heightPict);
    });
};

let heightImg = maxHeightImgSlaid(slaider.imgSlaids);
let widthImg = maxWidthImgSlaid(slaider.imgSlaids);
let finHeidhtImg = parseInt(widthImg / (widthImg / heightImg));
delegateHeight(slaider.tagSlaids, finHeidhtImg);

const mooveSlaid = (arrayButton) => {
    arrayButton.on('click', function() {
        let nameButton = this.dataset.arow;

        switch (nameButton) {

            case 'btnNext':

                if (counter > 0) {
                    slaider.tagContImage[counter].style.left = '-600px';
                    counter -= 1;
                    numberSlaid(counter);
                }
                break;

            case 'btnPrev':

                if (counter < numberCounter) {
                    slaider.tagContImage[counter + 1].style.left = '0px';
                    counter += 1;
                    numberSlaid(counter);
                }
                break;
        }
    });
};

$(window).on('resize', function () {
    setTimeout(function () {
        let heightImg = maxHeightImgSlaid(slaider.imgSlaids);
        let widthImg = maxWidthImgSlaid(slaider.imgSlaids);
        let finHeidhtImg = parseInt(widthImg / (widthImg / heightImg));
        delegateHeight(slaider.tagSlaids, finHeidhtImg);
    }, 500);
});

$(document).ready(function () {
    mooveSlaid(slaider.buttons);
});